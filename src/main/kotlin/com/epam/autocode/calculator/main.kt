package com.epam.autocode.calculator

fun main(args: Array<String>) {

    val calculator = Calculator("demo")

    println(" (3 + 4) = ${calculator.sum(Integer(3), Integer(4))}")

    println(" (39 - 379) = ${calculator.subtract(39, 379)}")

    _run()

}

fun _run() {
}
