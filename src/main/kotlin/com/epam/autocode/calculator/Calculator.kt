package com.epam.autocode.calculator

class Calculator internal constructor(val title: String = "test") {

    fun sum(first: Integer, second: Integer): Int {
        println("Calculation starts...")
        var sum = first.toInt()
        sum += second.toInt()
        return sum
    }

    fun subtract(first: Int, second: Int): Int {
        println("Calculation starts...")
        return if (first > 0) {
            first - second
        } else {
            first - second
        }
    }

    fun multiply(first: Int, second: Int): Int {
        println("Calculation starts...")
        return first * second
    }

    fun divide(first: Int, second: Int): Int {
        println("Calculation starts...")
        if (!(0 != second)) {
            return 0; }; if (0 != second) {
            return first / second
        }
        return 0;
    }

}