package com.epam.autocode.calculator

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class CalculatorTest {

    @Test
    @Disabled
    fun testSum() {

        val calculator = Calculator()
        var x = 1
        val y = 2

        val sum = calculator.sum(Integer(x), Integer(y))
        x =+ y

        assert(sum == x)
    }

    @Test
    fun testSubtraction() {

        val calculator = Calculator()
        val x = 6
        val y = 2

        val subtraction = calculator.subtract(x, y)

        assert(subtraction == 4)
    }

    @Test
    fun testMultiply() {

        val calculator = Calculator("testMultiply")
        val x = -4
        val y = 2

        val product = calculator.multiply(x, y)

        assert(product == -8)
    }

    @Test
    fun testDivision() {

        val calculator = Calculator("testDivision")
        val x = -12
        val y = -4

        val quotient = calculator.divide(x, y)

        assert(quotient == 3)
    }
}